package com.tema2

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request.Method.GET
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONArray
import org.json.JSONObject


class MainFragment : Fragment() {

    private lateinit var rootView: View

    private lateinit var firstName: EditText
    private lateinit var lastName: EditText

    private lateinit var recyclerView: RecyclerView

    private lateinit var userList: ArrayList<User>

    private lateinit var mQueue: RequestQueue

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        rootView = inflater.inflate(R.layout.fragment_main, container, false)

        firstName = rootView.findViewById(R.id.text_first_name)
        lastName = rootView.findViewById(R.id.text_last_name)


        val addButton: Button = rootView.findViewById(R.id.btn_add_user)
        addButton.setOnClickListener { addUser() }

        val deleteButton: Button = rootView.findViewById(R.id.btn_delete_user)
        deleteButton.setOnClickListener { deleteUser() }

        val syncButton: Button = rootView.findViewById(R.id.btn_sync)
        syncButton.setOnClickListener { sync() }

        userList = ArrayList()

        initializeRecyclerview()

        mQueue = Volley.newRequestQueue(context)

        return rootView
    }

    private fun addUser() {
        val name = firstName.text.toString() + " " + lastName.text.toString()

        userList.forEach {
            if (name == it.toString()) {
                Toast.makeText(
                    context,
                    "There is an already existing user with this name.",
                    Toast.LENGTH_SHORT
                ).show()
                return
            }
        }
        userList.add(User(firstName.text.toString(), lastName.text.toString()))
        recyclerView.adapter?.notifyDataSetChanged()

    }

    private fun deleteUser() {
        val name = firstName.text.toString() + " " + lastName.text.toString()

        userList.forEach {
            if (name == it.toString()) {
                userList.remove(it)
                recyclerView.adapter?.notifyDataSetChanged()
                return
            }
        }
        Toast.makeText(context, "Couldn't find any user.", Toast.LENGTH_SHORT).show()
    }


    private fun sync() {
        val queue = Volley.newRequestQueue(context)
        val url = "https://jsonplaceholder.typicode.com/users"

        val getConnectionRequest = StringRequest(
            GET, url,
            Response.Listener { response ->
                handleResponse(response.toString())
            },
            Response.ErrorListener {
                Toast.makeText(context, "IDI NAHUI", Toast.LENGTH_SHORT).show()
            })

        queue.add(getConnectionRequest)
    }

    private fun handleResponse(response: String) {
        var usersJson = JSONArray(response)

        for (i in 0 until usersJson.length()) {
            val user: JSONObject = usersJson[i] as JSONObject

            user?.let {
                var name = user["name"].toString()

                userList.add(User(name, ""))
            }
        }
        recyclerView.adapter?.notifyDataSetChanged()
    }

    private fun initializeRecyclerview() {
        recyclerView = rootView.findViewById(R.id.recyclerview)
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = ListAdapter(userList)
    }

}
