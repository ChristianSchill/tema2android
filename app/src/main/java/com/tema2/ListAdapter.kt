package com.tema2

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.recyclerview_element.view.*

class ListAdapter(userList: ArrayList<User>) : RecyclerView.Adapter<ListAdapter.MyViewHolder>() {

    private var mList= userList

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view : View = LayoutInflater.from(parent.context).inflate(R.layout.recyclerview_element,parent,false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mList.count()
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(mList[position])
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(user: User){
            itemView.text_user_name.text = user.toString()
        }
    }
}
