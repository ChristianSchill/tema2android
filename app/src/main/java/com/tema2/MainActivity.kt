package com.tema2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val transaction = this.supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frame_layout, MainFragment())
        transaction.commit()
    }
}
